
import { useEffect, useState } from "react";
import { Row, Button, Col, Input } from "reactstrap"

function Login() {

    const [email, setEmail] = useState();
    const [validateEmail, setValidateEmail] = useState()
    const [password, setPassword] = useState();
    const [validatePassword, setValidatePassword] = useState();
    
    
    const isValidEmail = email =>
        // eslint-disable-next-line no-useless-escape
        /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
            email
    );
  
    const onChangeEmail = (event) => {
        setEmail(event.target.value);
       
    }

    const onChangePassword = (event) => {
        setPassword(event.target.value)
    }

    const onBtnClickLogin = () => {
        setValidateEmail(isValidEmail(email))   
    }

    useEffect(() => {
        localStorage.setItem("email", email);
        localStorage.setItem("password", password);
    })

    useEffect(() => {
        document.title = "LogIn Form"
    }, [])

    return (
        <div>
            <Row>
                <h3 className="text-light mt-4">Wellcome Back</h3>
            </Row>
            <Row >
                <Col>
                    <Input value={email} type="email" className='mt-3 Ip' placeholder="Email Address" onChange={onChangeEmail}></Input>
                    {validateEmail === false && <p style={{color: "white"}}>Email ko hơp lệ</p>}
                    <Input type="password" value={password} className='mt-3 Ip' placeholder="Password" onChange={onChangePassword}></Input>

                </Col>
            </Row>
            <Row>
                <Col>
                    <Button active color="" block className="mt-4 BT text-light" size="lg" onClick={onBtnClickLogin}><b>LOG IN</b></Button>
                </Col>
            </Row>

        </div>
    )
}
export default Login;