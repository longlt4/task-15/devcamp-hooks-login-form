import { Row, Container, Button, Col,  } from "reactstrap"
import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect, useState } from "react";
import Login from "./Login";
import SignUp from "./SignUp";

function FormGroup() {
    const [displayIn, setDisplayIn] = useState(true)
    const [displaySign, setDisplaySign] = useState(false)
    const onBtnSignUp = () => {
        console.log("Sign Up")
        setDisplaySign(true)
        setDisplayIn(false)
    }
    const onBtnLogin = () => {
        console.log("Log in")
        setDisplayIn(true)
        setDisplaySign(false)
    }
    useEffect(() => {
        document.title = "LogIn Form"
    }, [])
    return (
        <div className=" text-center margin">
            <Container className="Form ">
                <Row >
                    <Col className="d-flex justify-content-center" >
                        <Button active color="" block className="mt-4 BT text-light " onClick={onBtnSignUp}>Sign Up</Button>
                        <Button active color="" block className="mt-4 BT text-light" onClick={onBtnLogin}>Log In</Button>
                    </Col>
                </Row>
                  { displayIn ? <Login  display={onBtnLogin}/> : null}
                  { displaySign ? <SignUp  display={onBtnSignUp}/> : null}
            </Container>    
        </div>
    )
}

export default FormGroup;