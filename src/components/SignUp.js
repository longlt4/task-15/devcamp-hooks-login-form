
import { useEffect, useState } from "react";
import { Row, Button, Col, Input } from "reactstrap"

function SignUp() {

    const [email, setEmail] = useState(localStorage.getItem("email") || "");
    const [password, setPassword] = useState(localStorage.getItem("password") || "");
    const [firstName, setFirstName] = useState(localStorage.getItem("firstname") || "");
    const [lastName, setLastName] = useState(localStorage.getItem("lastname") || "");

    const onChangeEmail = (event) => {
        setEmail(event.target.value);
    }
    const onChangePassword = (event) => {
        setPassword(event.target.value)
    }

    const onBtnClickSignUp = () => {
        console.log("FirstName: " + firstName)
        console.log("LastName: " + lastName)
        console.log("Email: " + email)
        console.log("Password: " + password)
    }

    const firstNameChangeHandler = (event) => {
        setFirstName(event.target.value);
    }

    const lastNameChangeHandler = (event) => {
        setLastName(event.target.value);
    }

    useEffect(() => {
        localStorage.setItem("email", email);
        localStorage.setItem("password", password);
        localStorage.setItem("firstname", firstName);
        localStorage.setItem("lastname", lastName);

    })

    useEffect(() => {
        document.title = "LogIn Form"
    }, [])

    return (
        <div>
            <Row>
                <h3 className="text-light mt-3">Sign Up For Free</h3>
            </Row>
            <Row >
                <Col>
                    <Input value={firstName} onChange={firstNameChangeHandler}className='mt-2 Ip' placeholder="First Name" ></Input>
                </Col>
                <Col>
                    <Input value={lastName} onChange={lastNameChangeHandler}  className='mt-2 Ip' placeholder="Last Name" ></Input>
                </Col>
            </Row>
            <Row >
                <Col>
                    <Input value={email} className='mt-2 Ip' placeholder="Email Address" onChange={onChangeEmail}></Input>
                    <Input  type="password" value={password} className='mt-2 Ip' placeholder="Password" onChange={onChangePassword}></Input>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Button active color="" block className="mt-4 BT text-light" size="lg" onClick={onBtnClickSignUp}><b>GET STARTED</b></Button>
                </Col>
            </Row>

        </div>
    )
}
export default SignUp;